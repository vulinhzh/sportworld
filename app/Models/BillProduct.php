<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property int    $id
 * @property int    $quantity
 * @property int    $bill_id
 * @property int    $quantity
 * @property int    $bill_id
 * @property float  $product_price
 * @property float  $product_price
 * @property string $product_title
 * @property string $product_title
 * @property Date   $sale_at
 * @property Date   $sale_at
 */
class BillProduct extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bill_product';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_price', 'product_title', 'quantity', 'sale_at', 'bill_id', 'product_price', 'product_title', 'quantity', 'sale_at', 'bill_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int', 'id' => 'int', 'product_price' => 'float', 'product_title' => 'string', 'quantity' => 'int', 'sale_at' => 'date', 'bill_id' => 'int', 'product_price' => 'float', 'product_title' => 'string', 'quantity' => 'int', 'sale_at' => 'date', 'bill_id' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'sale_at', 'sale_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Scopes...

    // Functions ...

    // Relations ...
}
