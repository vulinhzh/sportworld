<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int      $id
 * @property int      $id
 * @property int      $created_by
 * @property int      $updated_by
 * @property int      $created_by
 * @property int      $updated_by
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property DateTime $created_at
 * @property DateTime $updated_at
 * @property string   $description
 * @property string   $name
 * @property string   $url
 * @property string   $url_img
 * @property string   $description
 * @property string   $name
 * @property string   $url
 * @property string   $url_img
 */
class Banner extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banner';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by', 'created_at', 'updated_by', 'updated_at', 'description', 'name', 'url', 'url_img', 'created_by', 'created_at', 'updated_by', 'updated_at', 'description', 'name', 'url', 'url_img'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int', 'id' => 'int', 'created_by' => 'int', 'created_at' => 'datetime', 'updated_by' => 'int', 'updated_at' => 'datetime', 'description' => 'string', 'name' => 'string', 'url' => 'string', 'url_img' => 'string', 'created_by' => 'int', 'created_at' => 'datetime', 'updated_by' => 'int', 'updated_at' => 'datetime', 'description' => 'string', 'name' => 'string', 'url' => 'string', 'url_img' => 'string'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'created_at', 'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Scopes...

    // Functions ...

    // Relations ...
}
