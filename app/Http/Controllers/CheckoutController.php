<?php

namespace App\Http\Controllers;

use App\Models\TblBlogs;
use App\Models\TblCart;
use App\Models\TblProducts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckoutController extends Controller
{
    /////////////////------- Thêm sản phẩm vào giỏ hàng ------------///////////////
    public function choose_product_to_cart(Request $request) {
        $productId = $this->getProductId($request);
        $quantity = $this->getQuantity($request);
        $message;

        // kiểm tra người dùng đã đăng nhập
        if($request->session()->has('USER')){
            // lấy email
            $email = $request->session()->get('USER');
            $user = DB::table('users')->where('email', $email)->first();

            $cart = DB::table('cart')
                    ->where('product_id', $productId)
                    ->where('user_id', $user -> id)
                    ->first();
                    
            // trường hợp chưa đặt sản phẩm
            if ($cart == null){
                DB::table('cart')->insert(
                    array('product_id' => $productId, 'user_id' => $user->id, 'quantity' => $quantity)
                );
            }else{
                $soluong = DB::table('cart')
                            ->where('product_id', $productId)
                            ->where('user_id', $user -> id)
                            ->first()
                            ->quantity;
                if ($soluong + $quantity > 0){
                    DB::table('cart')
                        ->where('product_id', $productId)
                        ->where('user_id', $user -> id)
                        ->update(array('quantity' => $soluong + 1));
                } else
                {
                    DB::table('cart')
                            ->where('product_id', $productId)
                            ->where('user_id', $user -> id)
                            ->delete();
                }
            }
        }
        else{
            $carts = $request->session()->get('gCART');
            if ($carts == null){
                $carts = array();
            }
            // nếu không tồn tại key
            if (!array_key_exists("idsp".$productId, $carts)){
                $carts["idsp".$productId] = 0;
            }
            $carts["idsp".$productId] = $carts["idsp".$productId] + $quantity;
            //xóa mặt hàng khỏi giỏ khi số lượng <= 0
            if ($carts["idsp".$productId] <= 0){
                unset($carts["idsp".$productId]);
            }
            $request->session()->put('gCART', $carts);
        }
        $message = "Thêm giỏ hàng thành công";
        return view('front-end.thongbaoalert', ['message' => $message]);
    }
    private function getProductId(Request $request){
        return $request->productId;
    }
    private function getQuantity(Request $request){
        return $request->quantity;
    }


    ////////////////-------- load dữ liệu ra Giỏ hàng ---------- ///////////////////////
    public function shopping_cart(Request $request){
        // đã đăng nhập
        $carts = array();
        if($request->session()->has('USER')){
            // xử lý khi lần đầu đăng nhập, mà vẫn giữ giỏ hàng lúc chưa đăng nhập
            if ($request->session()->has('gCART')){
                $cartskhichualogin = $request->session()->get('gCART');
                if ($cartskhichualogin != null){
                    foreach ($cartskhichualogin as $key => $value)  {
                        $cartcosan = DB::table('cart')
                            ->where('product_id', (int)(explode("idsp", $key)[1]))
                            ->where('user_id', $this->GetUser($request)-> id)
                            ->first();
                        if ($cartcosan != null){
                            $soluong = ($value > $cartcosan->quantity)?$value:$cartcosan->quantity;
                            DB::table('cart')
                            ->where('product_id', product_id)
                            ->where('user_id', $this->GetUser($request)-> id)
                            ->update(array('quantity' => $soluong));
                        }else{
                            $soluong = $value;
                            DB::table('cart')
                            ->insert(
                                array(
                                    "product_id" => ((int)(explode("idsp", $key)[1])),
                                    'user_id' => $this->GetUser($request)-> id,
                                    'quantity' => $soluong
                                )
                            );
                        }
                    }
                    $request->session()->forget('gCART');
                }
            }



            $email = $request->session()->get('USER');
            $user = DB::table('users')->where('email', $email)->first();
            $carts = DB::table('cart')
            ->join('users', 'users.id', '=', 'cart.user_id')
            ->join('product', 'product.id', '=', 'cart.product_id')
            ->where("cart.user_id", '=', $user->id)
            ->select('product.url_avatar', 'product.id', 'product.title', 
            'product.price', 'cart.quantity'
            )
            ->get();
        }
        // trường hợp chưa đăng nhập
        else{
            $hangluutam = $request->session()->get('gCART');
            if ($hangluutam == null){
                $hangluutam = array();
            }
            foreach ($hangluutam as $key => $value) {
                $productId = (int)(explode("idsp", $key)[1]);
                $cart = DB::table('product')
                    ->where("id", '=', $productId)
                    ->select('url_avatar', 'id', 'title', 'price')
                    ->first();
                $cart -> quantity = $value;
                array_push($carts, $cart);
            }
        }
        $cate = "home";
        return view("front-end.shoping_cart", ["carts" => $carts, "cate" => $cate]);
    }

    ///////////////--------- Cập nhật giỏ hàng -------------//////////////////
    public function change_product_cart(Request $request){
        $productId = $this->getProductId($request);
        $quantity = $this->getQuantity($request);
        $message;

        // kiểm tra người dùng đã đăng nhập
        if($request->session()->has('USER')){
            // lấy email
            $email = $request->session()->get('USER');
            
            $user = DB::table('users')->where('email', $email)->first();

            // trường hợp chưa đặt sản phẩm
            if ($quantity > 0){
                DB::table('cart')
                    ->where('product_id', $productId)
                    ->where('user_id', $user -> id)
                    ->update(array('quantity' => $quantity));
            } else
            {
                DB::table('cart')
                        ->where('product_id', $productId)
                        ->where('user_id', $user -> id)
                        ->delete();
            }
        
        }
        else{
            $carts = $request->session()->get('gCART');
            
            // if ($carts == null){
            //     $carts = array();
            // }
            // // nếu tồn tại key
            $carts["idsp".$productId] = $quantity;
            
            //xóa mặt hàng khỏi giỏ khi số lượng <= 0
            if ($carts["idsp".$productId] <= 0){
                unset($carts["idsp".$productId]);
            }
            $request->session()->put('gCART', $carts);
        }
        $message = "Cập nhật giỏ hàng thành công";
        return view('front-end.thongbaoalert', ['message' => $message]);
    }

    
    ///////////////--------- Xóa hàng trong giỏ hàng -------------//////////////////
    public function remove_product(Request $request){
        $productId = $this->getProductId($request);
        $message;

        // kiểm tra người dùng đã đăng nhập
        if($request->session()->has('USER')){
            // lấy email
            $email = $request->session()->get('USER');
            
            $user = DB::table('users')->where('email', $email)->first();

            // trường hợp chưa đặt sản phẩm
            DB::table('cart')
                ->where('product_id', $productId)
                ->where('user_id', $user -> id)
                ->delete();
        }
        else{
            $carts = $request->session()->get('gCART');
            unset($carts["idsp".$productId]);
            $request->session()->put('gCART', $carts);
        }
        $message = "Xóa hàng trong giỏ hàng thành công";
        return view('front-end.thongbaoalert', ['message' => $message]);
    }

    //////////////---------- Hoá đơn  ---------------------//////////
    public function checkout(Request $request){
        if($request->session()->has('USER')){
            $carts = $this->GetCartsByUser($request);
            $user = $this->GetUser($request);
            $total = $this->GetTotalMoney($request);
            return view('front-end.checkout', ["carts" => $carts, "user" => $user, "total" => $total]);
        }else{
            return view('auth.login');
        }
    }
    private function GetUser(Request $request){
        $email = $request->session()->get('USER');
        $user = DB::table('users')->where('email', $email)->first();
        return $user;
    }
    private function GetCartsByUser(Request $request){
        $carts;
        if($request->session()->has('USER')){
            $email = $request->session()->get('USER');
            $user = DB::table('users')->where('email', $email)->first();
            $carts = DB::table('cart')
            ->join('users', 'users.id', '=', 'cart.user_id')
            ->join('product', 'product.id', '=', 'cart.product_id')
            ->where("cart.user_id", '=', $user->id)
            ->select('product.url_avatar', 'product.id', 'product.title', 
            'product.price', 'cart.quantity', "cart.user_id"
            )
            ->get();
        return $carts;
        }
    }
    private function GetTotalMoney(Request $request){
        $total = 0;
        $carts = $this->GetCartsByUser($request);
        
        foreach ($carts as $cart) {
            $total = $total + $cart->price * $cart -> quantity;
        }
        return $total;
    }


    ////// ------------- Lưu hóa đơn  --------------- /////////////
    public function save_cart(Request $request){

        $this->CapNhatDuLieuNguoiDungTuHoaDon($request);
        $thongtintuuser = $this->LayDuLieuTuHoaDon($request);
        $idHoaDon = DB::table('bill')->count() + 1;

        // thêm dữ liệu vào bảng bill
        DB::table('bill')->insert(
            array(
                'id' => $idHoaDon, 
                "code" => $this->GetDiscount($request),
                "customer_address" => $thongtintuuser["customerAddress"],
                "customer_email" => $thongtintuuser["customerEmail"],
                "customer_name" => $thongtintuuser["customerName"],
                "customer_note" => $thongtintuuser["customerNote"],
                "customer_phone" => $thongtintuuser["customerPhone"],
                "isCancel" => 0,
                
                "total" => $thongtintuuser["total"],
                "total_received" => $thongtintuuser["total_received"],
                "user_id" => $thongtintuuser["user_id"]
            )
        );

        
        $carts = $this->GetCartsByUser($request);
        foreach ($carts as $cart) {
            // thêm dữ liệu vào bảng bill_product
            DB::table('bill_product')->insert(
                array(
                    "product_price" => $cart->price,
                    "product_title" => $cart->title,
                    "quantity" => $cart->quantity,
                    "bill_id" => $idHoaDon
                )
            );
            //thêm dữ liệu vào bảng sale_product
            DB::table('sale_product')->insert(
                array(
                    "user_id" => $thongtintuuser["user_id"],
                    "product_id" => $cart->id,
                    "quantity" => $cart->quantity
                )
            );


            // xóa dữ liệu từ bảng cart
            DB::table('cart')
                ->where("product_id", $cart->id)
                ->where("user_id", $cart->user_id)
                ->delete();
        }
        $color = "green";
        $message = "Đã đặt đơn hàng";
        return view('front-end.thongbao', ["message" => $message, "color" => $color]);
    }
    private function LayDuLieuTuHoaDon(Request $request){
        $data = array();
        $data["customerName"] = $request->input("customerName");
        $data["customerAddress"] = $request->input("customerAddress");
        $data["customerPhone"] = $request->input("customerPhone");
        $data["customerEmail"] = $request->input("customerEmail");
        $data["customerNote"] = $request->input("customerNote");
        $data["discountName"] = $request->input("discount");


        $data["user_id"] = $this->GetUser($request)->id;
        $data["total"] = $this->GetTotalMoney($request);
        try{
            $data["total_received"] = $data["total"] - $data["total"] * $this->GetDiscount($request);
        }catch(Exception $e){
            $data["total_received"] = $data["total"];
        }
        return $data;
    }
    private function GetDiscount(Request $request){
        $discountName = $request->input("discount");
        $message = "";
        $discount = DB::table('discount')
            ->where('name', $discountName)->first();
        if ($discount == null){
           return 0;
        }else {
            return $discount->discount;
        }
    }
    private function CapNhatDuLieuNguoiDungTuHoaDon(Request $request){
        $thongtintuuser = $this->LayDuLieuTuHoaDon($request);
        $email = $request->session()->get('USER');
        $user = DB::table('users')->where('email', $email)->first();
        DB::table('users')
            ->where('id', $user->id)
            ->update(array('name' => $thongtintuuser["customerName"], 
                            'address' => $thongtintuuser["customerAddress"],
                            'phonenumber' => $thongtintuuser["customerPhone"]
            )
        );
    }


    ///// ------------ kiểm tra giảm giá -----------------////
    // trả về tỉ lệ được giảm, nếu không tồn tại mã trả về notfound
    public function check_discount(Request $request){
        $discountName = $request->discountName;;
        $message = "";
        $discount = DB::table('discount')
            ->where('name', $discountName)->first();
        if ($discount == null){
           $message = "notfound";
        }else {
            $message = $discount->discount;
        }
        return view('front-end.thongbaoalert', ["message" => $message]);
    }
    
}
