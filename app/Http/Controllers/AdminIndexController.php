<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\Bill;
use App\Models\TblUsers;
use App\Models\User;
use Illuminate\Http\Request;

class AdminIndexController extends Controller
{
    public function index(Request $request)
    {
        return view('back-end.index', [
            'qualityOfProduct'=> Product::count(), 'qualityOfUser'=> User::count()
            , 'qualityOfCategory'=>Category::count()
            , 'qualityOfSaleOrder'=> Bill::count()
        ]);
    }
}
