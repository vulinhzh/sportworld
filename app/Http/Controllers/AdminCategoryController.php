<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class AdminCategoryController extends Controller
{
    public function view()
    {
        $cate = Category::all();
        return view('back-end.view_categories', ['categories' => $cate]);
    }

    public function viewAdd()
    {
        $cate = new Category();
        return view('back-end.insert_category',['cate' => $cate]);
    }

    public function add(Request $request)
    {
        $id = $request->input('id');
        $name = $request->input('name');
        $description = $request->input('description');
        if ($id == null || strcmp($id, " ") == 0) {
            Category::create([
                'id' => null,
                'name' => $name,
                'description' => $description
            ]);
        } else {
            Category::where('id', $id)->update(['name' => $request->input('name')
            ,'description' => $request->input('description')]);
        }
        return redirect('/admin/categories');
    }

    public function delete($id) {
        Category::where('id', $id)->delete();
        return redirect('/admin/categories');
    }

    public function viewUpdate($id)
    {
        $cate = Category::where('id', $id)->first();
        return view('back-end.insert_category',['cate' => $cate]);
    }

    public function update(Request $request, $id) {
        Category::where('id', $id)->update(['name' => $request->input('name')
        ,'description' => $request->input('description')]);
        return redirect('/admin/categories');
    }
}
