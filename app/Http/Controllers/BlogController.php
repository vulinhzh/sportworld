<?php

namespace App\Http\Controllers;

use App\Models\Blogs;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function blog() 
    {
        $blogs = Blogs::all();
        return view('front-end.blog', ['blogs' => $blogs, 'cate' => 'blog']);
    }
}
