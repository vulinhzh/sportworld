<?php

namespace App\Http\Controllers;

use App\Models\BillProduct;
use App\Models\Bill;
use Illuminate\Http\Request;

class AdminOrderController extends Controller
{
    public function view()
    {
        $func = function($id) {
            return BillProduct::where('bill_id', $id)->get();
        };
        $orders = Bill::all();
        $ids = [];
        foreach ($orders as $order) {
            array_push($ids, $order->id);
        }
        $order_prod = array_map($func, $ids);
        return view('back-end.view_order',['order_prod'=>$order_prod, 'orders' => $orders]);
    }
}
