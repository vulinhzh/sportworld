<?php

namespace App\Http\Controllers;

use App\Models\Blogs;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class TestController extends Controller
{
    public function test(Request $request) {
        $message = bcrypt("12345678");
        return view('front-end.thongbao', ['message' => $message]);
    }
}
