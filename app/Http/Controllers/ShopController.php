<?php

namespace App\Http\Controllers;

use App\Models\ProductFilterModel;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\Tagsearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{

	public function detail($seo) {

		$product = DB::table('product')
		->select()
		->join('product_detail', 'product.id', '=', 'product_detail.product_id')
		->where('product.seo', '=', $seo)
		->first();
		$images = DB::table('product_images')
		->select()
		->where('product_id','=', $product->product_id)
		->get();
		$relatedProducts = Product::where('price','>=',$product->price - 100000 )->get();
		Product::where('seo', $seo)->update(['num_view' => $product->num_view + 1]);
		return view('front-end.detail', [
			'product' => $product, 'images' => $images, 'relatedProducts' => $relatedProducts
			, 'cate' => 'shop'
		]);
	}

    public function shop(Request $request) {
        $request->session()->forget('FILTER_MODEL');
        return redirect('shop-search?page=1');
    }

    public function shopSearch(Request $request) {
        $session = $request->session();
			
			if ($session->has('FILTER_MODEL')) {
				$proFilter = $session->get('FILTER_MODEL');
			} else {
				$proFilter = new ProductFilterModel();
				$proFilter->currenPage = 1;
                $proFilter->beginPrice = -1;
			}

			$strCategoryId = $request->input("categoryid");
			$strCurrentPage = $request->input("page");
			$strPriceBegin = $request->input("priceBegin");
			$strPriceEnd = $request->input("priceEnd");
			$strSort = $request->input("sort");
			$tag = $request->input("tag");

            if ($tag != null) {
                $proFilter->tag = $tag;
            }

			if ($strCategoryId != null) {
				$proFilter->categoryId = (int) $strCategoryId;
                if ($proFilter->categoryId == -1) $proFilter->categoryId = null;
				$proFilter->sort = null;
                $proFilter->tag = null;
			}
			
			if ($strPriceBegin != null) {
				$proFilter->beginPrice = (int) $strPriceBegin;
				$proFilter->sort = null;
                $proFilter->tag = null;
			}
			if ($strPriceEnd != null)
				$proFilter->endPrice = (int) $strPriceEnd;
			if ($strCurrentPage != null)
				$proFilter->currenPage = (int) $strCurrentPage;
			if ($strSort != null)
				$proFilter->sort = $strSort;
            $session->put('FILTER_MODEL', $proFilter);

            $products = $proFilter->filterProduct($proFilter);
            $categories = Category::all();
            $tags = Tagsearch::all();
			return view('front-end.shop',[
                'products' => $products, 'size' => $proFilter->size, 'totalPage' => $proFilter->totalPage
                , 'categories' => $categories, 'tags' => $tags, 'currentPage' => $proFilter->currenPage
                , 'currentCategoryId' => $proFilter->categoryId, 'cate' => 'shop', 'price' => $proFilter->beginPrice
            ]);
    }
}
