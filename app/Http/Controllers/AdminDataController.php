<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminDataController extends Controller
{
    public function index()
    {
        $all = "select product.id, product.title,product.price, product.price_old, sum(sale_product.quantity) as num_sale
        from sale_product join product on sale_product.product_id = product.id
        GROUP by product.id,product.title,product.price, product.price_old
        ORDER by num_sale desc
        LIMIT 10";

        $month = "select product.id, product.title,product.price, product.price_old, sum(sale_product.quantity) as num_sale
        from sale_product join product on sale_product.product_id = product.id
        WHERE month(sale_at) = month(now())
        GROUP by product.id,product.title,product.price, product.price_old
        ORDER by num_sale desc
        LIMIT 10";

        $view = "SELECT * FROM `product`
        ORDER by num_view DESC
        LIMIT 10";
        $products = DB::select($all);
        $month_product = DB::select($month);
        $view_products = DB::select($view);
        return view("back-end.data", ['products' => $products, 'month_products' => $month_product, 'view_products' => $view_products]);
    }
}
