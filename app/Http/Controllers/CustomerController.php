<?php

namespace App\Http\Controllers;

use App\Models\Bill;
use App\Models\BillProduct;
use App\Models\Blogs;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class CustomerController extends Controller
{
    /////////////// ---------------- trang index ------------------///////////////
    public function customer_index(Request $request) {
        if(!$request->session()->has('USER')){
            $color = "red";
            $message = "Bạn chưa đăng nhập!!";
            return view('front-end.thongbao', ['message' => $message, "color" => $color]);
        }
        $user = $this->GetUser($request);
        return view('back-end.customer.index', ['user' => $user]);
    }



    //////////// --------------- update lại mật khẩu --------------------///////////////
    public function customer_updatepassword(Request $request) {
        if(!$request->session()->has('USER')){
            return;
        }
        $oldPass = $request->input("oldPass");
        $newPass = $request->input("newPass");
        $user = $this->GetUser($request);

        // đúng mật khẩu
        if(password_verify($oldPass, $user->password)) {
            DB::table('users')
                ->where('id', $user -> id)
                ->update(array('password' => bcrypt($newPass)));
            $color = "green";
            $message = "Mật khẩu đã cập nhật!!";
        }else{
            $color = "red";
            $message = "Mật khẩu sai rồi!!";
        }
        
        //$user->password = \Hash::make($value);
        



        // if ($oldPass == $user->password){
        //     DB::table('users')
        //         ->where('user_id', $user->id)
        //         ->update(array('password' => $newPass));
        //     $message = "Cập nhật mật khẩu thành công!!";
        // }
        // else{
        //     $message = "Mật khẩu sai rồi!";
        // }

        return view('front-end.thongbao', ['message' => $message, "color" => $color]);
    }

    private function GetUser(Request $request){
        $email = $request->session()->get('USER');
        $user = DB::table('users')->where('email', $email)->first();
        return $user;
    }



    ///// -------------- mở trang cập nhật mật khẩu -----------------------///
    public function customer_password(Request $request) {
        if(!$request->session()->has('USER')){
            $color = "red";
            $message = "Bạn chưa đăng nhập!!";
            return view('front-end.thongbao', ['message' => $message, "color" => $color]);
        }
        return view('back-end.customer.changepassword');
    }



    ///// -------------- mở trang cập nhật thông tin -----------------------///
    public function customer_infomation(Request $request) {
        if(!$request->session()->has('USER')){
            $color = "red";
            $message = "Bạn chưa đăng nhập!!";
            return view('front-end.thongbao', ['message' => $message, "color" => $color]);
        }
        $user = $this->GetUser($request);
        return view('back-end.customer.update_info', ['user' => $user]);
    }      


    /////------------- cập nhật thông tin --------------------////////
    public function customer_updateinfomation(Request $request) {
        $name = $request->input("name");
        $email = $request->input("email");
        $sdt = $request->input("phone");
        $diachi = $request->input("address");
        if(!$request->session()->has('USER')){
            $color = "red";
            $message = "Bạn chưa đăng nhập!!";
            return view('front-end.thongbao', ['message' => $message, "color" => $color]);
        }
        DB::table('users')
            ->where('id', $this->GetUser($request)-> id)
            ->update(array(
                'name' => $name,
                "email" => $email,
                "address" => $diachi,
                "phonenumber" => $sdt
            )
        );
        $request->session()->put('USER', $email);
        $color = "green";
        $message = "Cập nhật thông tin thành công!!!!";
        return view('front-end.thongbao', ['message' => $message, "color" => $color]);
    }  


    /// ---------------------- xem orders -----------------------/////////
    public function view_orders(Request $request) {
        $func = function($id) {
            return BillProduct::where('bill_id', $id)->get();
        };
        // $orders = DB::table('bill')
        //                 ->where('user_id', $this->GetUser($request)-> id);
        $orders = Bill::where('user_id',  $this->GetUser($request)-> id)->get();
        $ids = [];
        foreach ($orders as $order) {
            array_push($ids, $order->id);
        }
        $order_prod = array_map($func, $ids);
        return view('back-end.customer.view_orders',['order_prod'=>$order_prod, 'orders' => $orders]);
    } 
}
