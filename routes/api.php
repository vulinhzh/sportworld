<?php

use App\Http\Controllers\Api\BillController;
use App\Http\Controllers\Api\DataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('admin/is-pay/{id}', [BillController::class, 'update']);
Route::get('admin/is-cancel/{id}', [BillController::class, 'cancel']);
Route::get('calculate', [DataController::class, 'calculateChar']);