<!DOCTYPE html>
<html lang="en">
<head>
    @include('back-end.common.css')
    <link href="http://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
    .add{
    	padding: 20px 10px;
    	width: 150px;
    	float:right;
    	border: 1px solid black;
    	border-radius: 15px;
    	color: black;
    	background-color: white;
    	margin-bottom: 20px;
    	font-weight: bolder;
    	
    }
    
    .add:hover{
    	background-color:black;
    	color:white;
    	transition: 0.5s;
    }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <div class="dashboard">
                @include('back-end.common.menu')
                <div class="right">
                    <div class="right__content">
                    <div class="right__title">Top 10 sản phẩm được xem nhiều nhất</div>
                        <div class="right__table">
                            <div class="right__tableWrapper">
                                <table id="myTable" style="border:none;">
                                    <thead>
                                        <tr>
                                            <th scope="col">STT</th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Tên sản phẩm</th>
                                            <th scope="col">Giá bán</th>
                                            <th scope="col">Giá Cũ</th>
                                            <th scope="col">Số lượt xem</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($view_products as $product)
	                                        <tr>
	                                            <td data-label="STT" scope="row">{{ $loop->iteration }}</td>
                                                <td data-label="Tên sản phẩm">{{$product->id}}</td>
	                                            <td data-label="Tên sản phẩm">{{$product->title}}</td>
	                                            <td data-label="Giá SP">{{number_format($product->price,3)}}</td>
	                                            <td data-label="Giá Sale">{{number_format($product->price_old,3)}}</td>
	                                            <td data-label="Đã bán">{{$product->num_view}}</td>
                                            </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <div class="right__title">Top 10 sản phẩm bán chạy nhất trong tháng:</div>
                        <div class="right__table">
                            <div class="right__tableWrapper">
                                <table id="myTable" style="border:none;">
                                    <thead>
                                        <tr>
                                            <th scope="col">STT</th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Tên sản phẩm</th>
                                            <th scope="col">Giá bán</th>
                                            <th scope="col">Giá Cũ</th>
                                            <th scope="col">Đã bán</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($month_products as $product)
	                                        <tr>
	                                            <td data-label="STT" scope="row">{{ $loop->iteration }}</td>
                                                <td data-label="Tên sản phẩm">{{$product->id}}</td>
	                                            <td data-label="Tên sản phẩm">{{$product->title}}</td>
	                                            <td data-label="Giá SP">{{number_format($product->price,3)}}</td>
	                                            <td data-label="Giá Sale">{{number_format($product->price_old,3)}}</td>
	                                            <td data-label="Đã bán">{{$product->num_sale}}</td>
                                            </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="right__title">Top 10 sản phẩm bán chạy nhất:</div>
                        <div class="right__table">
                            <div class="right__tableWrapper">
                                <table id="myTable" style="border:none;">
                                    <thead>
                                        <tr>
                                            <th scope="col">STT</th>
                                            <th scope="col">ID</th>
                                            <th scope="col">Tên sản phẩm</th>
                                            <th scope="col">Giá bán</th>
                                            <th scope="col">Giá Cũ</th>
                                            <th scope="col">Đã bán</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)
	                                        <tr>
	                                            <td data-label="STT" scope="row">{{ $loop->iteration }}</td>
                                                <td data-label="Tên sản phẩm">{{$product->id}}</td>
	                                            <td data-label="Tên sản phẩm">{{$product->title}}</td>
	                                            <td data-label="Giá SP">{{number_format($product->price,3)}}</td>
	                                            <td data-label="Giá Sale">{{number_format($product->price_old,3)}}</td>
	                                            <td data-label="Đã bán">{{$product->num_sale}}</td>
                                            </tr>
                                        @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('back-end.common.js')
    <script type="text/javascript" src="http://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script>
		function myFunction(id){
			  var r = confirm("Bạn có muốn xoá sản phẩm này ?");
			  if (r == true) {
			    window.location = "/admin/product-delete/" + id;
			  } else {
			    window.location = "/admin/products";
			  }
		}
		
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
	</script>
</body>
</html>