<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/views/back-end/customer/common/css.jsp"></jsp:include>
    @include('front-end.common.css')
</head>
<body>

    <div class="wrapper">
        <div class="container">
            <div class="dashboard">
                <jsp:include page="/WEB-INF/views/back-end/customer/common/menu.jsp"></jsp:include>
                <div class="right">
                    <div class="right__content">
                        <h6 class="checkout__title">Bảng điều khiển</h6>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="checkout__input">
                                        <p>
                                            Tên của bạn<span></span>
                                        </p>
                                        <input id="customerName" name="customerName"  value="{{$user->name }}" required = "required" readonly="readonly"/>
                                    </div>
                                    <div class="checkout__input">
                                        <p>
                                            Email<span></span>
                                        </p>
                                        <input id="customerName" name="customerName"  value="{{$user->email }}" required = "required" readonly="readonly"/>
                                    </div>
                                    <div class="checkout__input">
                                        <p>
                                            Số điện thoại<span></span>
                                        </p>
                                        <input id="customerName" name="customerName"  value="{{$user->phonenumber }}" required = "required" readonly="readonly"/>
                                    </div>
                                    <div class="checkout__input">
                                        <p>
                                            Địa chỉ<span></span>
                                        </p>
                                        <input id="customerName" name="customerName"  value="{{$user->address }}" required = "required" readonly="readonly"/>
                                    </div>
                                </div>
                            </div>
                        </h6>
                        <a style="float:left;margin-left:100;" href="/customer-infomation"><button style="padding: 0 20px;" class="site-btn btn">Cập nhật thông tin</button></a>
                        <a style="float:left; margin-right:350px;" href="/customer-password"><button style="padding: 0 20px;"class="site-btn btn">Đổi mật khẩu</button></a>
                        <a style="float:left; margin-right:600px;" href="/view-orders"><button style="padding: 0 20px;"class="site-btn btn">Xem orders</button></a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
	<jsp:include page="/WEB-INF/views/back-end/common/js.jsp"></jsp:include>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/8.1.2/es-modules/parts/Chart.min.js"></script>
	<script type="text/javascript">
		<c:if test="${not empty exist}">
			alert("Tên đăng nhập đã tồn tại!");
		</c:if>
	</script>
</body>
</html>