<!DOCTYPE html>
<html lang="en">
<head>	
	@include('front-end.common.css')
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <div class="dashboard">
                <jsp:include page="/WEB-INF/views/back-end/customer/common/menu.jsp"></jsp:include>
                <div class="right">
                    <div class="right__content">
						<h6 class="checkout__title">Cập nhật mật khẩu</h6> </h6>    
                        <div class="right__formWrapper">
                            <form action="/customer-updatepassword" method="get" >
							<div class="checkout__input">
									<p>
									Mật khẩu cũ<span></span>
									</p>
									<input type="password" id="oldPass" name="oldPass" required = "required"/>
								</div>
								<div class="checkout__input">
									<p>
									Mật khẩu mới<span></span>
									</p>
									<input type="password" id="newPass" name="newPass" required = "required"/>
								</div>
								<div class="checkout__input">
									<p>
									Xác nhận mật khẩu mới<span></span>
									</p>
									<input type="password" id="validateNewPass" name="validateNewPass" required = "required"/>
								</div>
                                <button class="btn site-btn" type="submit">Lưu</button>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
	<jsp:include page="/WEB-INF/views/back-end/common/js.jsp"></jsp:include>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/8.1.2/es-modules/parts/Chart.min.js"></script>
	<script>
	function KiemTraXacNhanMatKhau(){
		if ($('#password').val() != $('#confirm_password').val()) {
			alert("Mật khẩu và xác nhận mật khẩu không khớp");
		  }
	}
	$("#password").change(function(){
		alert("The text has been changed.");
	});
	</script>
</body>
</html>