<!DOCTYPE html>
<html lang="en">
<head>
    @include('front-end.common.css')
    <style type="text/css">
    	.er {
    		color :red;
    	}
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <div class="dashboard">
                <jsp:include page="/WEB-INF/views/back-end/customer/common/menu.jsp"></jsp:include>
                <div class="right">
                    <div class="right__content">
                        <h6 class="checkout__title">Thông tin cá nhân</h6> </h6>    
                            <form action="/customer-updateinfomation" method="get" modelAttribute="user" >
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="checkout__input">
                                            <p>
                                                Tên của bạn<span></span>
                                            </p>
                                            <input id="name" name="name"  value="{{$user->name }}" required = "required"/>
                                        </div>
                                        <div class="checkout__input">
                                            <p>
                                                Email<span></span>
                                            </p>
                                            <input id="email" name="email"  value="{{$user->email }}" required = "required"/>
                                        </div>
                                        <div class="checkout__input">
                                            <p>
                                                Số điện thoại<span></span>
                                            </p>
                                            <input id="phone" name="phone"  value="{{$user->phonenumber }}" required = "required"/>
                                        </div>
                                        <div class="checkout__input">
                                            <p>
                                                Địa chỉ<span></span>
                                            </p>
                                            <input id="address" name="address"  value="{{$user->address }}" required = "required"/>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn site-btn" type="submit">Cập nhật</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
	<jsp:include page="/WEB-INF/views/back-end/common/js.jsp"></jsp:include>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/8.1.2/es-modules/parts/Chart.min.js"></script>
	
</body>
</html>