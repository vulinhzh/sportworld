<!DOCTYPE html>
<html lang="en">
<head>
@include('back-end.common.css')
    <link href="http://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet">
</head>
<body>
    <div class="wrapper">
        <div class="container">
            <div class="dashboard">
                 <jsp:include page="/WEB-INF/views/back-end/customer/common/menu.jsp"></jsp:include>
                 <div class="right">
                    <div class="right__content">
                        <div class="right__title">Bảng điều khiển</div>
                        <p class="right__desc">Xem đơn hàng</p>
                        <div class="right__table">
                            <div class="right__tableWrapper">
                                <table id="myTable">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Họ Tên</th>
                                            <th>SĐT</th>
                                            <th>Stt sp</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Số lượng</th>
                                            <th>Giá tiền</th>
                                            <th>Ngày</th>
                                            <th>Tổng tiền</th>
                                            <th>Địa chỉ</th>
                                            <th>Trạng thái</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @for($i=0;$i < count($orders); $i++) 
                                            <tr>
                                            <td data-label="STT" rowspan="{{count($order_prod[$i]) + 1}}">{{$i + 1}}</td>
                                            <td data-label="Tên" rowspan="{{count($order_prod[$i]) + 1}}">{{$orders[$i]->customer_name}}</td>
                                            <td data-label="phone" rowspan="{{count($order_prod[$i]) + 1}}">{{$orders[$i]->customer_phone}}</td>
                                            </tr>
                                            <tr>
                                                <td data-label="Stt sp">1</td>
                                                <td data-label="Tên sản phẩm">{{$order_prod[$i][0]->product_title}}</td>
                                                <td data-label="Số lượng">{{$order_prod[$i][0]->quantity}}</td>
                                                <td data-label="Giá tiền">{{$order_prod[$i][0]->product_price}}</td>

                                                <td data-label="Ngày" rowspan="{{count($order_prod[$i]) + 1}}">{{$orders[$i]->created_date}}</td>
                                                <td data-label="Tổng" rowspan="{{count($order_prod[$i]) + 1}}">{{$orders[$i]->total}}</td>
                                                <td data-label="Địa chỉ" rowspan="{{count($order_prod[$i]) + 1}}">{{$orders[$i]->customer_address}}</td>

                                                <td data-label="Trạng thái" rowspan="{{count($order_prod[$i]) + 1}}">
                                                    <span id="status-{{$orders[$i]->id}}" class="badge badge-primary">{{$orders[$i]->status}}</span>
                                                </td>

                                                
                                            </tr>
                                            @for($j=1;$j< count($order_prod[$i]); $j++) <tr>
                                                <td data-label="Stt sp">{{$j + 1}}</td>
                                                <td data-label="Tên sản phẩm">{{$order_prod[$i][$j]->product_title}}</td>
                                                <td data-label="Số lượng">{{$order_prod[$i][0]->quantity}}</td>
                                                <td data-label="Giá tiền">{{$order_prod[$i][0]->product_price}}</td>
                                                </tr>
                                                @endfor
                                                @endfor
                                    </tbody>
                                </table>
                                <a href="/customer-index">Quay lại</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('back-end.common.js')
	<script type="text/javascript" src="http://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
</body>
</html>