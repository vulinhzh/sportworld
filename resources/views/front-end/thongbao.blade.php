
<!DOCTYPE html>
<html>
<head>
    <title>Thông báo</title>
    <style>
    	.CartIsEmpty{
    		text-align:center;
    		font-size:30px;
    		font-weight:bolder;
    		text-transform: uppercase; 
    		color:green;
    		margin-bottom: 40px;
    	}
    </style>
	@include('front-end.common.css')
</head>
<body>

    <section class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__text">
                        <h4>Thông báo</h4>
                        <div class="breadcrumb__links">
                            <a href="/">Trang chủ</a>
                            <img src="/images/right-arrow.png">
                            <span>Thông báo</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="shopping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shopping__cart__table">
                        <div class="CartIsEmpty" style="color: {{$color}};">{{$message}}</div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="continue__btn">
                                <!-- <a href="${pageContext.request.contextPath}/shop">Tiếp tục mua sắm</a> -->
                                <a href="/shop">Tiếp tục mua sắm</a>
                            </div>
                        </div>
                        
                    </div>
                </div>
               </div>
        </div>
    </section>
    <script type="text/javascript">
    	<c:if test="${not empty checkout}">
    		window.confirm("Đặt hàng thành công!");
    	</c:if>
    </script>
	@include('front-end.common.js')
</body>
</html>